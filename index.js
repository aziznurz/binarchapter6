const express = require("express");
const modulesSite = require("./modules/site/index");
const modulesUser = require("./modules/user/user");
const users = require("./data/users.json");
const { User_game, User_game_biodata } = require("./models");
const app = express();
const bodyParser = require("body-parser");
const user_game = require("./models/user_game");
const port = 3000;

app.set("view engine", "ejs");

app.use(express.static("assets"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.get("/login", (req, res) => {
  res.render("login.ejs");
});
app.post("/login", (req, res) => {
  const { username, password } = req.body;

  for (user of users) {
    if (username === user.username && password === user.password) {
      return res.redirect("/dashboard");
    }
  }

  res.status(400).send('<h1 style="display: flex; justify-content: center; color: red;">Wrong Email or Password</h1>');
});

app.get("/", modulesSite.home);

app.get("/games", modulesSite.games);

app.get("/user", modulesUser.list);

app.get("/dashboard", async (req, res) => {
  const user_games = await User_game.findAll();
  res.render("dashboard.ejs", {
    user_games: user_games,
  });
});

app.get("/dashboard/create", (req, res) => {
  res.render("create.ejs");
});

app.post("/dashboard", async (req, res) => {
  if (req.body.id) {
    await User_game.update(req.body, {
      where: {
        id: req.body.id,
      },
    });
  } else {
    await User_game.create(req.body);
  }
  res.redirect("/dashboard");
});

app.get("/dashboard/:id", async (req, res) => {
  const user_game = await User_game.findOne({
    where: {
      id: req.params.id,
    },
  });
  res.render("biodata.ejs", { user_game: user_game });
});

app.get("/dashboard/:id/edit", async (req, res) => {
  const user_game = await User_game.findOne({
    where: {
      id: req.params.id,
    },
  });
  res.render("edit.ejs", { user_game: user_game });
});

app.get("/dashboard/:id/delete", async (req, res) => {
  await User_game.destroy({
    where: {
      id: req.params.id,
    },
  });
  res.redirect("/dashboard");
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
